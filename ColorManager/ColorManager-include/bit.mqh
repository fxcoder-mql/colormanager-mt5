/*
Copyright 2023 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Bit functions. © FXcoder

class CBitUtil
{
public:

	static bool check(int value, int bits) { return (value & bits) != 0; }
	static int  set  (int value, int bits) { return value | bits;        }
	static int  clear(int value, int bits) { return value & (~bits);     }

} _bit;
