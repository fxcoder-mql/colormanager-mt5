/*
Copyright 2023 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Файловые функции. © FXcoder

#include "file/file_text.mqh"
#include "bit.mqh"

class CFileUtil
{
public:

	/*
	Считать текстовый файл по строкам в массив строк.
	@param path       Путь до файла.
	@param lines[]    Ссылка на массив строк, в который будут загружены строки файла.
	@param flags      Дополнительные флаги для FileOpen.
	@return           Количество загруженных строк (размер массива strings[]) или -1 в случае ошибки.
	*/
	static int read_lines(string path, int flags, string &lines[])
	{
		CFileText file(path);

		if (!file.open(FILE_READ | FILE_TXT | flags))
			return -1;

		return file.read_lines(lines);
	}

	static bool write_lines(string path, int flags, const string &lines[])
	{
		CFileText file(path);
		if (!file.open(FILE_WRITE | FILE_TXT | flags))
			return false;

		const bool append = _bit.check(flags, FILE_READ);
		if (append)
			file.seek_end();

		return file.write_lines(lines);
	}

	static bool exists(const string path, bool common_folder = false)
	{
		// Проверка на существование (не использовать (bool), т.к. в случае неудачи возвращается -1
		return 1 == FileGetInteger( path, FILE_EXISTS, common_folder);
	}

} _file;
