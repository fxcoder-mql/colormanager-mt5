/*
Copyright 2023 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к файлу. © FXcoder

class CFile
{
protected:

	const string path_;
	int handle_;
	int flags_;

public:

	void CFile(string path):
		path_(path),
		handle_(INVALID_HANDLE)
	{
	}

	void ~CFile()
	{
		close();
	}

	virtual bool open(int flags)
	{
		flags_ = flags;
		handle_ = FileOpen(path_, flags);
		return is_open();
	}

	bool is_open() const { return handle_ != INVALID_HANDLE; }


	// Закрытие файла.
	// Файл также принудительно закрывается при удалении этого объекта.
	void close()
	{
		if (is_open())
		{
			FileClose(handle_);
			handle_ = INVALID_HANDLE;
		}
	}


	void  flush          () const {        FileFlush(handle_);             } // Сброс на диск всех данных, оставшихся в файловом буфере ввода-вывода.
	bool  is_ending      () const { return FileIsEnding(handle_);          } // Определяет конец файла в процессе чтения.
	bool  is_line_ending () const { return FileIsLineEnding(handle_);      } // Определяет конец строки в текстовом файле в процессе чтения.
	bool  seek_end       () const { return FileSeek(handle_, 0, SEEK_END); }
	ulong tell           () const { return FileTell(handle_); }

	bool  seek (long offset, ENUM_FILE_POSITION origin) const { return FileSeek(handle_, offset, origin); }

	// Свойства
	bool     exists      () const { return (bool)     FileGetInteger( handle_, FILE_EXISTS      ); } // Проверка на существование
	datetime create_date () const { return (datetime) FileGetInteger( handle_, FILE_CREATE_DATE ); } // Дата создания
	datetime modify_date () const { return (datetime) FileGetInteger( handle_, FILE_MODIFY_DATE ); } // Дата последнего изменения
	datetime access_date () const { return (datetime) FileGetInteger( handle_, FILE_ACCESS_DATE ); } // Дата последнего доступа к файлу
	long     size        () const { return (long)     FileGetInteger( handle_, FILE_SIZE        ); } // Размер файла в байтах
	long     position    () const { return (long)     FileGetInteger( handle_, FILE_POSITION    ); } // Позиция указателя в файле
	bool     end         () const { return (bool)     FileGetInteger( handle_, FILE_END         ); } // Получение признака конца файла
	bool     line_end    () const { return (bool)     FileGetInteger( handle_, FILE_LINE_END    ); } // Получение признака конца строки
	bool     is_common   () const { return (bool)     FileGetInteger( handle_, FILE_IS_COMMON   ); } // Файл открыт в общей папке всех клиентских терминалов (смотри FILE_COMMON)
	bool     is_text     () const { return (bool)     FileGetInteger( handle_, FILE_IS_TEXT     ); } // Файл открыт как текстовый (смотри FILE_TXT)
	bool     is_binary   () const { return (bool)     FileGetInteger( handle_, FILE_IS_BINARY   ); } // Файл открыт как бинарный (смотри FILE_BIN)
	bool     is_csv      () const { return (bool)     FileGetInteger( handle_, FILE_IS_CSV      ); } // Файл открыт как CSV (смотри FILE_CSV)
	bool     is_ansi     () const { return (bool)     FileGetInteger( handle_, FILE_IS_ANSI     ); } // Файл открыт как ANSI (смотри FILE_ANSI)
	bool     is_readable () const { return (bool)     FileGetInteger( handle_, FILE_IS_READABLE ); } // Файл открыт с возможностью чтения (смотри FILE_READ)
	bool     is_writable () const { return (bool)     FileGetInteger( handle_, FILE_IS_WRITABLE ); } // Файл открыт с возможностью записи (смотри FILE_WRITE)
};
