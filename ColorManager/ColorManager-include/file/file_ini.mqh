/*
Copyright 2023 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// INI-файл. © FXcoder


/*
1. Комментарии только на всю строку, строка начинается с `;`.
2. Если переменная повторяется, то будет использовано последнее значение.
3. Параметры trim_* указывают необходимость обрезки строк, значений, названий секций и переменных.
4. В строгом режиме все строки и значения считываются как есть, иначе строки, секции, имена и значения
     будут обрезаны (убраны пробельные символы на концах)

Пример:
; comment
[Group]
variable=value
*/


#include <Generic/HashMap.mqh>
#include "../file.mqh"
#include "../str.mqh"


class CIniFile: protected CHashMap<string, CHashMap<string, string>*>
{
private:

	bool is_ready_;

public:

	void CIniFile()
	{
		is_ready_ = init();
	}

	void ~CIniFile()
	{
		clear();
	}

	bool init()
	{
		if (!clear())
			return false;

		CHashMap<string, string> *section;
		return get_section_global(section);
	}

	bool clear()
	{
		const int count = Count();

		string names[];
		CHashMap<string, string>* sections[];
		if (CopyTo(names, sections) != count)
			return false;

		for (int i = 0; i < count; ++i)
			delete sections[i];

		Clear();
		return true;
	}

	// Does nos delete global section.
	// Deletes all occurrences.
	// Returns true if smth deleted.
	bool delete_section(string name)
	{
		if (!is_ready_)
			return false;

		sanitize_str(name);
		if (name == "")
			return false;

		CHashMap<string, string>* section;
		if (!TryGetValue(name, section))
			return false;

		delete section;
		return Remove(name);
	}

	bool load(const string &lines[])
	{
		if (!init())
			return false;

		const int nlines = ArraySize(lines);

		// global section by default
		CHashMap<string, string> *section;
		if (!get_section_global(section))
			return false;

		string section_name = "";

		for (int i = 0; i < nlines; i++)
		{
			string line = _str.trim(lines[i]);
			int len = StringLen(line);

			// empty line or comment
			if (len <= 0 || line[0] == ';')
				continue;

			// new section
			if ((line[0] == '[') && (line[len - 1] == ']'))
			{
				section_name = _str.trim(StringSubstr(line, 1, len - 2));
				if (section_name == "")
				{
					Print("ERROR: Section name cannot be empty");
					return false;
				}

				continue;
			}

			// value
			int eq_pos = StringFind(line, "=");
			if (eq_pos < 0)
			{
				Print("Wrong line: [" + line + "]");
				return false;
			}

			string name = _str.trim(StringSubstr(line, 0, eq_pos));
			string value = _str.trim(StringSubstr(line, eq_pos + 1));
			if (!set(name, section_name, value))
				return false;
		}

		return true;
	}

	bool load(string path, int flags)
	{
		string lines[];
		int nlines = _file.read_lines(path, flags, lines);
		if (nlines <= 0)
			return false;

		return load(lines);
	}

	bool get_section_names(string &section_names[])
	{
		CHashMap<string, string>* sections[]; // dummy
		return CopyTo(section_names, sections) == Count();
	}

	bool get_lines(string &lines[])
	{
		if (!is_ready_)
			return false;

		const int section_count = Count();

		string section_names[];
		CHashMap<string, string>* sections[];
		if (CopyTo(section_names, sections) != section_count)
			return false;

		CLinkedList<string> line_list;

		for (int i = 0; i < section_count; ++i)
		{
			const string section_name = section_names[i];

			CHashMap<string, string> *section;
			if (!TryGetValue(section_name, section))
				return false;

			const int pair_count = section.Count();

			// skip empty sections
			if (pair_count == 0)
				continue;

			// section header
			if (section_name != "")
			{
				if (!line_list.Add("[" + section_name + "]"))
					return false;
			}

			// pairs
			{
				string names[], values[];
				if (section.CopyTo(names, values) != pair_count)
					return false;

				for (int j = 0; j < pair_count; ++j)
				{
					if (!line_list.Add(names[j] + "=" + values[j]))
						return false;
				}
			}

			// section delimiter
			line_list.Add("");
		}

		const int line_count = line_list.Count();
		if (ArrayResize(lines, line_count) != line_count)
			return false;

		if (line_list.CopyTo(lines) != line_count)
			return false;

		return true;
	}

	bool save(string path, int flags)
	{
		string header[];
		return save(path, flags, header);
	}

	bool save(string path, int flags, const string &header[])
	{
		string lines[];
		if (!get_lines(lines))
			return false;

		if (ArraySize(header) != 0)
		{
			if (!ArrayInsert(lines, header, 0))
				return false;
		}

		return _file.write_lines(path, flags, lines);
	}

// type, parser
#define _FILEINI_GET(T, P) T get(string name, string section, T fallback) {  \
	string value_str;                                                        \
	return get_str(name, section, value_str)                                 \
		? P(value_str) : fallback; }

	_FILEINI_GET(long, StringToInteger)
	_FILEINI_GET(double, StringToDouble)
	_FILEINI_GET(color, StringToColor)
	_FILEINI_GET(string, string)

#define _FILEINI_SET(T) bool set(string name, string section, T value) {  \
	return set_t(name, section, value); }

	_FILEINI_SET(long)
	_FILEINI_SET(double)
	_FILEINI_SET(color)
	_FILEINI_SET(string)

private:

	void sanitize_str(string &s)
	{
		if (s == NULL)
		{
			s = "";
		}
		else
		{
			StringTrimLeft(s);
			StringTrimRight(s);
		}
	}

	bool get_section(string name, CHashMap<string, string>* &section)
	{
		sanitize_str(name);

		if (ContainsKey(name))
			return TryGetValue(name, section);

		CHashMap<string, string>* new_section = new CHashMap<string, string>();
		if (!Add(name, new_section))
			return false;

		section = new_section;
		return true;
	}

	bool get_section_global(CHashMap<string, string>* &section)
	{
		return get_section("", section);
	}

	template<typename T>
	bool set_t(string name, string section_name, T value)
	{
		sanitize_str(section_name);
		CHashMap<string, string>* section;
		if (!get_section(section_name, section))
			return false;

		sanitize_str(name);
		string value_str = (string)value;
		sanitize_str(value_str);
		return section.TrySetValue(name, value_str);
	}

	bool get_str(string name, string section_name, string &value)
	{
		sanitize_str(section_name);
		CHashMap<string, string>* section;
		if (!get_section(section_name, section))
			return false;

		sanitize_str(name);
		return section.TryGetValue(name, value);
	}

};

void __test_inifile()
{
}
