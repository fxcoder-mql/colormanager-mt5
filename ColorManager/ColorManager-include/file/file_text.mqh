/*
Copyright 2023 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к текстовому файлу. © FXcoder

#include <Generic/LinkedList.mqh>
#include "../bit.mqh"
#include "file.mqh"


class CFileText: public CFile
{
private:

	int char_size_;

public:

	void CFileText(string path):
		CFile(path)
	{
	}

	virtual bool open(int flags) override
	{
		flags_ = FILE_TXT | flags;

		// FILE_ANSI | FILE_UNICODE => Unicode
		// FILE_ANSI                => ANSI
		//             FILE_UNICODE => Unicode
		//                          => Unicode
		char_size_ = (_bit.check(flags_, FILE_ANSI) && !_bit.check(flags_, FILE_UNICODE)) ? 1 : 2;
		return CFile::open(flags_);
	}

	// См. параметры для FileWriteString, параметр length на практике не нужен
	uint write(string s)
	{
		return FileWrite(handle_, s);
	}

	// См. параметры для FileWriteString, параметр length на практике не нужен
	uint write_line(string s)
	{
		return FileWriteString(handle_, s);
	}

	// См. параметры для FileReadString, параметр length на практике не нужен
	string read_line()
	{
		return FileReadString(handle_);
	}

	// Сохранить массив строк в текстовый файл построчно.
	bool write_lines(const string &lines[])
	{
		for (int i = 0, count = ArraySize(lines); i < count; i++)
		{
			string line = lines[i] + "\r\n";
			uint size = char_size_ * (StringLen(line) + 2); // 2 is "\r\n"
			uint written_size = write(line);

			if (written_size != size)
				return false;
		}

		return true;
	}

	bool read_lines(string &lines[])
	{
		CLinkedList<string> list;

		while (!is_ending())
		{
			string line = read_line();

			if (!list.Add(line))
				return false;
		}

		const int count = list.Count();
		if (ArraySize(lines) > count &&
			ArrayResize(lines, 0, count) != 0)
			return false;

		if (list.CopyTo(lines) != count)
			return false;

		return true;
	}
};
